﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="index.aspx.cs" Inherits="Spiroglyphics.Index" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Spiraalpuzzelgenerator</title>
    <meta name="robots" content="noindex" />
    <script type="text/javascript">
        function uploadBmp() {
            document.getElementById("imgSvg").style.visibility = "hidden";
            __doPostBack();
        }
    </script>
</head>
<body style="margin:0">
    <form id="myForm" runat="server">
        <asp:ScriptManager ID="myScriptManager" runat="server"/>
        <div style="max-width:100%;width:100vh;max-height:100vh;font-size:0;position:relative;float:left">
            <asp:Image ID="imgSvg" runat="server"/>
            <asp:FileUpload ID="fulBmp" ClientIDMode="Static" runat="server" AllowMultiple="false" onchange="uploadBmp()" style="position:absolute;top:0;left:0;bottom:0;width:100%;opacity:0"/>
        </div>
        <div>
            <h1 style="margin-top:0">Spiraalpuzzelgenerator</h1>
            <asp:TextBox Text="" ID="tbxKey" runat="server" onkeydown="return(event.keyCode!=13)"/>
            <p>Denk aan het geheime wachtwoord</p>
            <p>Sleep een plaatje op de spiraal, of klik op de spiraal voor een dialoogvenster</p>
            <asp:HyperLink ID="hlkSvg" Target="_blank" download runat="server">DOWNLOAD</asp:HyperLink>
        </div>
    </form>
</body>
</html>
